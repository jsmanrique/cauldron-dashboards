[![Contributor Covenant](https://img.shields.io/badge/Contributor%20Covenant-v2.0%20adopted-ff69b4.svg)](code_of_conduct.md) 
| <a href="https://ilovefs.org"><img src="https://fsfe.org/campaigns/ilovefs/artwork/graphics/ilovefs-button-small-en.png" style="border: 0 !important;" alt="I love Free Software!"></a>

# My Cauldron Dashboards

This repository hosts some dashboards I've created for [Cauldron](https://cauldron.io).

# How to use them

> **Note**: While [Cauldron issue 214](https://gitlab.com/cauldronio/cauldron/issues/214) is not solved, remember that you need 
[to set up your private workspace](https://community.cauldron.io/t/prepare-your-workspace-required-steps-before-building-custom-visualizations-and-dashboards/20) first.

If you clone this repository, you'll have a set of some sort of `.json` files in
the dashboards folder. You can also download those that you want directly from 
this repo.

These files are the definition (or `design`) of dashboards. You can import one of this files in your Cauldron project workspace using:

`Management` > `Saved Objects` > `Import object`:

<img src="screenshots/kibana-management.jpg" alt="Kibana Management menu item" width="250"> >>> <img src="screenshots/kibana-saved-objects.jpg" alt="Kibana Saved Objects menu item" width="250"> >>> <img src="screenshots/kibana-import-button.jpg" alt="Import button" width="640">

Select file and import it:

<img src="screenshots/kibana-import.jpg" alt="Import button" width="640">

If everything goes well, *voilà*. You should have a new dashboard called *CHAOSS Metrics* listed in your `Dashboards` menu option:

<img src="screenshots/cauldron-kibana-chaoss-metrics-dashboard.jpg" alt="CHAOSS Metrics in the list of Kibana dashboards" width="640">

If you have any issue, please, let me know by opening an issue in this repository, or feel free to ask in [Cauldron Community forum](https://community.cauldron.io)

# Dashaboards

## CHAOSS Metrics
| Description | File | Screenshot |
|-------------|------|------------|
|A set of [CHAOSS Metrics](https://chaoss.community/metrics) dashboard| File: [chaoss-metrics.ndjson](dashboards/chaoss-metrics.ndjson) | <img src="screenshots/Screenshot-CHAOSS-Metrics.png" alt="CHAOSS Metrics dashboard screenshot" width="720">|
|A dashboard to analyze active code authros and their commits depending on the timezone and or email domain| File: [disruption-analysis.ndjson](dashboards/disruption-analysis.ndjson)| <img src="screenshots/disruption-analysis.png" alt="Zombie Apocalypse Dashboard" width="720"> |
|Dashboard to help on contributors information curation (affiliation, merging identities) on Cauldron Cloud| File:[contributors-affiliations.ndjson](dashboards/contributors-affiliations.ndjson)|<img src="screenshots/contributors-affiliations.jpg" width="720"> |

# License and Technologist's oath

<a href=")](https://opensource.org)"><img src="https://opensource.org/files/OSIApprovedCropped.png" width="120" alt="Open Source Initiative Approved License"></a> | [GPLv3](LICENSE)

[Technologist's Oath](ethics.md)
